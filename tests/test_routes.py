from app import app


def test_home_route():
    app.testing = True
    client = app.test_client()

    response = client.get('/')
    assert b'Hello World!' == response.data
