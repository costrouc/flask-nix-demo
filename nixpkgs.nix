# to update: $ nix-prefetch-url --unpack url
import (builtins.fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/e4395211d2f41cf4e3e92ddda2e1185e40335a5e.tar.gz";
  sha256 = "033rs7m8ix22nws6m49f61h0v5wxxk366aciii17y2rabjlk0w0a";
})
