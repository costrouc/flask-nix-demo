# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="flask-nix-demo",
    version="0.0.1",
    description="Flask Nix Example",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/costrouc/flask-nix-demo',
    author='Chris Ostrouchov',
    author_email='chris.ostrouchov+flask-nix-demo@gmail.com',
    license="MIT",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    keywords='python nix nixpkgs flask',
    packages=find_packages(exclude=['docs', 'tests']),
    # >38.6.0 needed for markdown README.md
    setup_requires=['pytest-runner', 'setuptools>=38.6.0'],
    tests_require=['pytest', 'pytest-cov'],
    install_requires=['flask'],
    entry_points={
        'console_scripts': [
            'flask-nix=app.__main__:main'
        ]
    }
)
