{ nixpkgs ? import <nixpkgs> {} }:

let inherit (nixpkgs) pkgs;
in
pkgs.stdenv.mkDerivation {
  name = "flask-nix-demo-env-0";
  buildInputs = [ pkgs.python36Packages.flask ];

}
