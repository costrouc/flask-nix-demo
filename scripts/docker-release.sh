set -e

IMAGE_NAME="$CI_REGISTRY/$CI_PROJECT_PATH"
IMAGE_TAG=${CI_COMMIT_TAG:-latest}

export DOCKER_BUILD=$(nix-build release.nix -A docker --option sandbox true --argstr imageName "$IMAGE_NAME" --argstr imageTag "$IMAGE_TAG")
echo "build" $DOCKER_BUILD

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
docker load --input $DOCKER_BUILD
docker push "$IMAGE_NAME:$IMAGE_TAG";
