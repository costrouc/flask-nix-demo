NIX_BUILD_OPTIONS=--option sandbox true

test:
	nix-build release.nix -A test	 ${NIX_BUILD_OPTIONS}

docker-release:
	nix run -f nixpkgs.nix docker -c scripts/docker-release.sh
