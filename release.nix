{ imageName ? "flask-nix-demo", imageTag ? "latest" }:
let
  pkgs = (import ./nixpkgs.nix { config = {}; overlays = []; }).pkgs;
  basename = "flask-nix-demo";
  version = "0.0.1";
  pythonPackages = pkgs.python36Packages;

  jobs = rec {
     test =
       pythonPackages.buildPythonPackage rec {
          pname = basename;
          inherit version;

          src = ./.;

          buildInputs = [ pythonPackages.setuptools ];
          checkInputs = [
             pythonPackages.pytestrunner
             pythonPackages.pytest
             pythonPackages.pytestcov
          ];
          propagatedBuildInputs = [ pythonPackages.flask ];

          checkPhase = ''
            py.test
          '';
       };
     docker = pkgs.dockerTools.buildImage {
       name = "${imageName}";
       tag = "${imageTag}";

       contents = jobs.test;

       config = {
         Cmd = [ "/bin/flask-nix" ];
       };
     };
  };
in
  jobs
